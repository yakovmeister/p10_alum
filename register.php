<?php
/**
 * Alumni Monitoring System
 * ------------------------
 * NOTE: remove this file if you want user to manually register an alumni.
 * @author Jacob Baring <owari@waifu.club>
 * @version 1.0.0
 */

session_start();

require('vendor/autoload.php');

use App\Database;
use App\Notification\SMS;
use App\Model\Alumni;

// initialize our connection
$connection = new Database;
$smsConfig = getConfig()['sms'];
$sms = new SMS($smsConfig['email'], $smsConfig['password']);

$message = $smsConfig['message'];

$is_authenticated = is_authenticated();

if ($is_authenticated) 
{
	header('location: /?page=alumni');
}

if (!!count($_POST)) 
{
	// Congratulations, your input has an email!
	if ($_POST['email'])
	{
		$record = Alumni::where('email', $_POST['email'])->get();

		if (!!count($record))
		{
			$_SESSION['alert'] = 'danger';
			$_SESSION['message'] = 'Email is already used.';
		} else {
			$_POST = array_map(function($item) {
				return htmlentities($item);
			}, $_POST);

			$status = Alumni::create($_POST);

			if ($status)
			{
				$_SESSION['alert'] = 'success';
				$_SESSION['message'] = 'You have successfully registered!';
			}	
		}
	}
	else
	{
		$_POST = array_map(function($item) {
			return htmlentities($item);
		}, $_POST);

		$status = Alumni::create($_POST);

		if ($_POST['contact'])
		{
			$devices = $sms->getDevices();

			if ($devices['response']['success'])
			{
				$data = $devices['response']['result']['data'][0];

				  $sms->sendMessageToNumber($_POST['contact'], $message, $data['id'], []);
			}
		}

		if ($status)
		{
			$_SESSION['alert'] = 'success';
			$_SESSION['message'] = 'You have successfully registered!';
		}
	}
}

?>

<?php /** Our base HTML Template */ ?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-reboot.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/fontawesome.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	</head>
	<body class="app-container">
		<div class="registration">
			<h2>Register as Alumni</h2>
			<?php if (isset($_SESSION['message'])) { ?>
				<div
					class="alert alert-<?php echo $_SESSION['alert'] ?>"
					role="alert"
				>
				  <?php echo $_SESSION['message'] ?>
				  <button
				  	type="button"
				  	class="close"
				  	data-dismiss="alert"
				  	aria-label="Close"
				  >
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			<?php unset($_SESSION['message']); } ?>
			<form method="POST">
				<div class="registration-form">
					<div class="registration-element">
						<div class="input-group input-group-sm mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">First name</span>
						  </div>
						  <input
						  	type="text"
						  	class="form-control"
						  	name="first_name"
						  	id="first_name"
						  >
						</div>
						<div class="input-group input-group-sm mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Middle name</span>
						  </div>
						  <input
						  	type="text"
						  	class="form-control"
						  	name="middle_name"
						  	id="middle_name"
						  >
						</div>
						<div class="input-group input-group-sm mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Last name</span>
						  </div>
						  <input
						  	type="text"
						  	class="form-control"
						  	name="last_name"
						  	id="last_name"
						  >
						</div>
						<div class="input-group input-group-sm mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Male</span>
						    <div class="input-group-text">
						      <input
						      	type="radio"
						      	id="gender"
						      	name="gender"
						      	value="male"
						      >
						    </div>
						    <span class="input-group-text">Female</span>
						    <div class="input-group-text">
						      <input
						      	type="radio"
						      	id="gender"
						      	name="gender"
						      	value="female"
						      >
						    </div>
						  </div>
						 </div>
						<div class="input-group input-group-sm mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Email</span>
						  </div>
						  <input
						  	type="text"
						  	class="form-control"
						  	name="email"
						  	id="email"
						  >
						</div>
						<div class="input-group input-group-sm mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Phone</span>
						  </div>
						  <input
						  	type="text"
						  	class="form-control"
						  	name="contact"
						  	id="contact"
						  >
						</div>
					</div>
					<div class="registration-element">
						<div class="input-group input-group-sm mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Year Graduated</span>
						  </div>
						  <input
						  	type="text"
						  	class="form-control"
						  	name="graduated_on"
						  	id="graduated_on"
						  >
						</div>
					  <div class="input-group  input-group-sm mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Awards</span>
						  </div>
						  <textarea
						  	class="form-control"
						  	id="awards"
						  	name="awards"
						  >
						  </textarea>
						</div>
						<div class="input-group input-group-sm mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Employment Status</span>
						  </div>
						  <input
						  	type="text"
						  	class="form-control"
						  	name="employment_status"
						  	id="employment_status"
						  >
						</div>
						<div class="input-group input-group-sm mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Job</span>
						  </div>
						  <input
						  	type="text"
						  	class="form-control"
						  	name="job"
						  	id="job"
						  >
						</div>
						<div class="input-group input-group-sm mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Company</span>
						  </div>
						  <input
						  	type="text"
						  	class="form-control"
						  	name="company"
						  	id="company"
						  >
						</div>
						<div class="input-group input-group-sm mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Salary</span>
						  </div>
						  <input
						  	type="text"
						  	class="form-control"
						  	name="salary"
						  	id="salary"
						  >
						</div>
					</div>
				</div>
				<input
					type="submit"
					class="btn btn-primary btn-block"
					name="register"
					value="Register"
				/>
			</form>
		</div>
		<script type="text/javascript" src="assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/js/popper.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	</body>
</html>