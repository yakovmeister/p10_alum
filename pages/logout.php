<?php

unset($_SESSION["app-token"]);
unset($_SESSION['uid']);

$_SESSION['alert'] = "success";
$_SESSION['message'] = "Successfully Logged out!";

header("location: /?page=index");
exit();