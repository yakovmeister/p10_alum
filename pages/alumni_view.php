<?php

use App\Model\Alumni;

if (!$_GET['id']) 
{
	header('location: /?page=alumni');
}

$record = Alumni::find($_GET['id']);

if (!$record)
{
	$_SESSION['message'] = 'Couldn\'t find specified alumni.';
	$_SESSION['alert'] = 'warning';
	header('location: /?page=alumni');
}

?>

<div class="view-container">
	<div class="view-section">
		<div class="view-header-section">
			<h1 align="center"># <?php echo $record->id ?></h1>
			<h3 align="center">
				<?php echo $record->first_name.' '
					.$record->middle_name.' '
					.$record->last_name 
				?>
			</h3>
			<p align="center"><?php echo $record->email ?> | <?php echo $record->contact ?></p>
			<p align="center">Graduated on: <?php echo $record->graduated_on ?></p>
			<div class="magical-buttons">
				<a
					href="/?page=alumni_edit&id=<?php echo $record->id ?>"
					class="btn btn-primary btn-sm"
				>
					<span class="fa fa-edit"></span>
					Edit
				</a>
				<a
					href="/?page=announce&id=<?php echo $record->id ?>"
					class="btn btn-success btn-sm"
				>
					<span class="fa fa-bullhorn"></span>
					Send Announcement
				</a>
				<a
					href="/?page=alumni_delete&id=<?php echo $record->id ?>"
					class="btn btn-danger btn-sm"
				>
					<span class="fa fa-trash"></span>
					Delete
				</a>
			</div>	
		</div>
		<div>
			<div class="view-section-element">
				<p><b>Gender:</b> <?php echo $record->gender ?></p>
				<p><b>Awards:</b> <?php echo $record->awards ?></p>
				<p><b>Employment Status:</b> <?php echo $record->employment_status ?></p>
				<p><b>Company:</b> <?php echo $record->company ?></p>
				<p><b>Nature of Job:</b> <?php echo $record->job ?></p>
				<p><b>Salary:</b> <?php echo $record->salary ?></p>
				<p><b>Added on:</b> <?php echo $record->created_at ?></p>
			</div>
		</div>
	</div>

</div>