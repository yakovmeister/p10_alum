<?php

use App\Model\Alumni;

if (!!count($_POST)) 
{
	// Congratulations, your input has an email!
	if ($_POST['email'])
	{
		$record = Alumni::where('email', $_POST['email'])->get();

		if (!!count($record))
		{
			$_SESSION['alert'] = 'danger';
			$_SESSION['message'] = 'Email is already used.';
		} else {
			$_POST = array_map(function($item) {
				return htmlentities($item);
			}, $_POST);

			$status = Alumni::create($_POST);

			if ($status)
			{
				$_SESSION['alert'] = 'success';
				$_SESSION['message'] = 'You have successfully registered!';
			}	
		}
	}
	else
	{
		$_POST = array_map(function($item) {
			return htmlentities($item);
		}, $_POST);

		$status = Alumni::create($_POST);

		if ($status)
		{
			$_SESSION['alert'] = 'success';
			$_SESSION['message'] = 'You have successfully registered!';
		}
	}
}

?>

<div class="app-container">
	<div class="registration">
		<h2>Register an Alumni</h2>
		<?php if (isset($_SESSION['message'])) { ?>
			<div
				class="alert alert-<?php echo $_SESSION['alert'] ?>"
				role="alert"
			>
			  <?php echo $_SESSION['message'] ?>
			  <button
			  	type="button"
			  	class="close"
			  	data-dismiss="alert"
			  	aria-label="Close"
			  >
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
		<?php unset($_SESSION['message']); } ?>
		<form method="POST">
			<div class="registration-form">
				<div class="registration-element">
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">First name</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="first_name"
					  	id="first_name"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Middle name</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="middle_name"
					  	id="middle_name"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Last name</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="last_name"
					  	id="last_name"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Male</span>
					    <div class="input-group-text">
					      <input
					      	type="radio"
					      	id="gender"
					      	name="gender"
					      	value="male"
					      >
					    </div>
					    <span class="input-group-text">Female</span>
					    <div class="input-group-text">
					      <input
					      	type="radio"
					      	id="gender"
					      	name="gender"
					      	value="female"
					      >
					    </div>
					  </div>
					 </div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Email</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="email"
					  	id="email"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Phone</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="contact"
					  	id="contact"
					  >
					</div>
				</div>
				<div class="registration-element">
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Year Graduated</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="graduated_on"
					  	id="graduated_on"
					  >
					</div>
				  <div class="input-group  input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Awards</span>
					  </div>
					  <textarea
					  	class="form-control"
					  	id="awards"
					  	name="awards"
					  ></textarea>
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Employment Status</span>
						</div>
						<select
							class="form-control"
							id="employment_status"
							name="employment_status"
						>					
							<?php if ($alumni->employment_status === 'permanent') { ?>
								<option value="permanent" selected>Permanent</option>
							<?php } else { ?>
								<option value="permanent">Permanent</option>
							<?php } ?>
							
							<?php if ($alumni->employment_status === 'temporary') { ?>
								<option value="temporary" selected>Temporary</option>
							<?php } else { ?>
								<option value="temporary">Temporary</option>
							<?php } ?>
						</select>
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Job</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="job"
					  	id="job"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Company</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="company"
					  	id="company"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Salary</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="salary"
					  	id="salary"
					  >
					</div>
				</div>
			</div>
			<input
				type="submit"
				class="btn btn-primary btn-block"
				name="register"
				value="Submit"
			/>
		</form>
	</div>
</div>
		