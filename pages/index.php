<?php
	use App\Model\Alumni;


	// overall total employment_statuss	
	$alumni = $connection->db()->table('alumni')
		->select('employment_status', $connection->db()->raw('count(*) as total'))
		->groupBy('employment_status')
		->get(['employment_status']);

	$yearly = $connection->db()->table('alumni')
		->select('employment_status', $connection->db()->raw('count(*) as total'), 'graduated_on')
		->groupBy('graduated_on')
		->get();

	$yearlyPerma = $connection->db()->table('alumni')
		->select('employment_status', $connection->db()->raw('count(*) as total'), 'graduated_on')
		->groupBy('graduated_on')
		->where('employment_status', 'permanent')
		->get();

	$yearlyTemp = $connection->db()->table('alumni')
		->select('employment_status', $connection->db()->raw('count(*) as total'), 'graduated_on')
		->groupBy('graduated_on')
		->where('employment_status', 'temporary')
		->get();
?>	

<div class="dashboard">
	<div class="dashboard-header">
		<h1>Dashboard</h1>
	</div>
	<div class="dashboard-content">
		<div class="dashboard-element">
			<h3 align="center">Employment Status Graph</h3>
			<div id="status"></div>
		</div>
		<div class="dashboard-element">
			<h3 align="center">Graduated</h3>
			<div id="yearly"></div>
		</div>
	</div>
	<div class="dashboard-content">
		<div class="dashboard-element">
			<h3 align="center">Employment Status<br/>(permanent)</h3>
			<div id="yearly-grad"></div>
		</div>
		<div class="dashboard-element">
			<h3 align="center">Employment Status<br/>(temporary)</h3>
			<div id="yearly-gradtmp"></div>
		</div>
	</div>
</div>



<script type="text/javascript" src="assets/js/chartist.min.js"></script>
<script type="text/javascript">

	// Create a new line chart object where as first parameter we pass in a selector
	// that is resolving to our chart container element. The Second parameter
	// is the actual data object.
	var data = {
	  labels: [],
	  series: []
	}

	<?php foreach($alumni as $alumnus) { ?>
		data.labels.push("<?php echo $alumnus['employment_status'] ?>")
		data.series.push("<?php echo $alumnus['total'] ?>")
	<?php } ?>

	var yearlyData = {
		labels: [],
		series: []
	}

	var yearlyGradData = {
		labels: [],
		series: []
	}

	var yearlyGradTmpData = {
		labels: [],
		series: []
	}

	<?php foreach($yearly as $year) { ?>
		yearlyData.labels.push("<?php echo $year['graduated_on'] ?>")
		yearlyData.series.push("<?php echo $year['total'] ?>")
	<?php } ?>

	<?php foreach($yearlyPerma as $year) { ?>
		yearlyGradData.labels.push("<?php echo $year['graduated_on'] ?>")
		yearlyGradData.series.push("<?php echo $year['total'] ?>")
	<?php } ?>

	<?php foreach($yearlyTemp as $year) { ?>
		yearlyGradTmpData.labels.push("<?php echo $year['graduated_on'] ?>")
		yearlyGradTmpData.series.push("<?php echo $year['total'] ?>")
	<?php } ?>


	var options = {
	  labelInterpolationFnc: function(value) {
	    return value[0]
	  },
	  width: '330px',
	  height: '330px',
	  chartPadding: 5
	};

	var responsiveOptions = [
	  ['screen and (min-width: 640px)', {
	    labelDirection: 'explode',
	    labelInterpolationFnc: function(value) {
	      return value;
	    }
	  }]
	];

	new Chartist.Pie('#status', data, options, responsiveOptions);
	new Chartist.Pie('#yearly', yearlyData, options, responsiveOptions);
	new Chartist.Pie('#yearly-grad', yearlyGradData, options, responsiveOptions);
	new Chartist.Pie('#yearly-gradtmp', yearlyGradTmpData, options, responsiveOptions);
</script>