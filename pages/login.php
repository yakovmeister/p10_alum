<?php
	if (!!count($_POST))
	{
		$status = authenticate($_POST['email'], $_POST['password']);

		if (!$status) 
		{
			$_SESSION['alert'] = 'danger';
			$_SESSION['message'] = 'Email or password may be incorrect.';
		}
		else
		{
			header('location: /?page=index');
			exit();
		}
	}
?>

<form
	method="POST"
	class="app-container bg-primary"
>
	<div class="login-container bg-light">
		<div class="app-logo"></div>
		<div class="app-login-container">
			<?php if ($_SESSION['message']) { ?>
				<div
					class="alert alert-<?php echo $_SESSION['alert'] ?>"
					role="alert"
				>
				  <?php echo $_SESSION['message'] ?>
				  <button
				  	type="button"
				  	class="close"
				  	data-dismiss="alert"
				  	aria-label="Close"
				  >
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			<?php unset($_SESSION['message']); } ?>
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <span
			    	class="input-group-text"
			    	id="user"
			    >
			    	<span class="fa fa-user"></span>
			    </span>
			  </div>
			  <input
			  	type="text"
			  	name="email"
			  	class="form-control"
			  	placeholder="e.g. john53@gmail.com"
			  	aria-label="Email"
			  />
			</div>
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <span
			    	class="input-group-text"
			    	id="password"
			    >
						<span class="fa fa-key"></span>
			    </span>
			  </div>
			  <input
			  	type="password"
			  	name="password"
			  	class="form-control"
			  	placeholder="Password"
			  	aria-label="Password">
			</div>
				<a href="/recover.php">Forgot Password?</a>
				<input
					type="submit"
					class="btn btn-primary btn-block"
					name="login"
					value="Login"
				/>
		</div>
	</div>
	<div class="sidenotch">
		<footer>version 0.1.0</footer>
		<!-- &copy; yakovmeister 2018 -->
	</div>
</form>
