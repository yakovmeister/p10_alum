<?php

use \PhpOffice\PhpSpreadsheet\IOFactory;
use App\Model\Alumni;

$target_dir = './resources/upload';
$tmp_name = bin2hex(openssl_random_pseudo_bytes(10));

if ($_FILES['xls']) 
{
    $ext = explode('.', $_FILES["xls"]['name']);
    $ext = $ext[count($ext) - 1];
    $filenamae = $target_dir.'/'.$tmp_name.'.'.$ext;

    @move_uploaded_file($_FILES["xls"]["tmp_name"], $filenamae);

    if (file_exists($filenamae))
    {
        $spreadsheet = IOFactory::load($filenamae);

        $worksheet = $spreadsheet->getActiveSheet();

        $rows = [];
        foreach ($worksheet->getRowIterator() AS $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);
            $cells = [];
            foreach ($cellIterator as $cell) {
                $cells[] = $cell->getValue();
            }
            $rows[] = trim($cells);
        }
    }

    $header = $rows[0];
    $newRow = array_slice($rows, 1);
    $items = [];

    foreach($newRow as $row)
    {
    	$rowIndex = [];

      foreach ($row as $index => $value)
      {
      	$rowIndex[$header[$index]] = $value;
      }

      array_push($items, $rowIndex);
    }

    foreach($items as $item)
    {
    		// hell no! that's not identical
    		if (!Alumni::where('email', $item['email'])->first())
    		{
	        Alumni::create($item);
    		}
    }

    @unlink($filenamae);

    $_SESSION['alert'] = 'success';
    $_SESSION['message'] = 'Import success!';
    header('location: /?page=alumni');
    exit();
}
?>

<div class="view-container">
    <div class="view-header">
        <h1>Import XLSX/CSV</h1>
    </div>
    <div class="view-information-section">
        <form method="POST" enctype="multipart/form-data">
            <p >Select a file to upload (Maximum 25Mb):</p>
            <div>
                <input type="file" name="xls" id="xls">
            </div>
            <div>
                <input type="submit" value="Import file" name="submit" class="btn btn-primary btn-block">
            </div>
        </form>
    </div>
</div>