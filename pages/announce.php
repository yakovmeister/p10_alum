<?php

use App\Notification\SMS;
use App\Model\Alumni;

$smsConfig = getConfig()['sms'];
$sms = new SMS($smsConfig['email'], $smsConfig['password']);

if ($_GET['id'])
{
    $receivers = Alumni::where('id', $_GET['id'])->get();

    if (strlen($receivers[0]->contact) < 1)
    {
        $_SESSION['message'] = 'Record doesn\'t have a number associated with';
        $_SESSION['alert'] = 'danger';
        header('location: /?page=alumni');
        exit();
    }
}
else
{
    $receivers = Alumni::whereNotNull('contact')->get();    
}

if($_POST)
{
    foreach ($receivers as $receiver)
    {
        $result = smsSend($receiver->contact, $_POST['message']);
    }

     if ($result == ""){
        $_SESSION['message'] = 'Failed to send messages';
        $_SESSION['alert'] = 'warning';
    }

    else if ($result == 0){        
        $_SESSION['message'] = 'PM sent';
        $_SESSION['alert'] = 'success';
    }

    else
    {
        $_SESSION['message'] = 'Error: '.$result.' has been encountered';
        $_SESSION['alert'] = 'warning';
    }
}

?>

<div class="view-container">
    <?php if (isset($_SESSION['message'])) { ?>
        <div
            class="alert alert-<?php echo $_SESSION['alert'] ?>"
            role="alert"
        >
          <?php echo $_SESSION['message'] ?>
          <button
            type="button"
            class="close"
            data-dismiss="alert"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    <?php unset($_SESSION['message']); } ?>
    <div class="view-header">
        <h1>Announce</h1>
    </div>
    <div class="view-information-section">

        <form method="POST">
            <p>Send an announcement to <?php echo $_GET['id'] ? $receivers[0]->first_name : 'everyone' ?>:</p>
            <div>
                <textarea name="message" id="message" class="form-control" placeholder="Message..."></textarea>
            </div>
            <div>
                <input type="submit" value="Send" name="submit" class="btn btn-primary btn-block">
            </div>
        </form>
    </div>
    
</div>