<div class="app-container">
	<div class="app-not-found">
		<h1>Oops!</h1>
		<p>The page you are looking for does not exist.</p>
		<a href="/?page=index" class="btn btn-primary">
		<span class="fa fa-angle-double-left"></span> Take me home.</a>
	</div>
</div>