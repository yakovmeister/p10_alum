<?php

use App\Model\Alumni;
use App\Model\User;

if (!$_GET['id'])
{
    $_SESSION['alert'] = 'warning';
    $_SESSION['message'] = 'Specified resources does not exist.';
    header('location: /?page=alumni');
    exit();
}

$alumni = Alumni::find($_GET['id']);

if (!count($alumni))
{
    $_SESSION['alert'] = 'warning';
    $_SESSION['message'] = 'Specified resources does not exist.';
    header('location: /?page=alumni');
    exit();
}

if ($user = User::where('alumni_id', $alumni->id)->first())
{
	$user->delete();
}

$alumni->delete();

$_SESSION['alert'] = 'success';
$_SESSION['message'] = "Delete Success!";
header('location: /?page=alumni');
exit();