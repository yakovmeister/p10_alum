<?php
	use App\Model\Alumni;

	$take = $_GET['perPage'] ? $_GET['perPage'] : 20;
	$skip = $_GET['from'] ? $_GET['from'] : 1;
	$skipped = ($skip - 1) * $take;
	$count = Alumni::count();

	$alumni = Alumni::skip($skipped)->take($take)->get();

	$alumni = $alumni->map(function ($item) {
		$you = $item['id'] === $_SESSION['uid'] ? ' <span class="badge badge-primary">you</span>' : '';
		$user = $item->user->id ? ' <span class="badge badge-success">user</span>' : '';
		$data = [
			'id' => '<td>'.$item['id'].'</td>',
			'name' => '<td>'.$item['first_name'].' '.$item['last_name'].$user.$you.'</td>',
			'email' => '<td>'.$item['email'].'</td>',
			'contact' => '<td>'.$item['contact'].'</td>',
			'view' => '<td><a href="/?page=alumni_view&id='.$item['id'].'">View</a></td>'
		];

		return implode('', $data);
	});

	$header = [
		['key' => 'id', 'value' => '#'],
		['key' => 'name', 'value' => 'Name'],
		['key' => 'email', 'value' => 'Email'],
		['key' => 'contact', 'value' => 'Contact'],
		['key' => 'action', 'value' => 'Action']
	];

	$parsedHeader = array_map(function($item) {
		return '<th scope="col">'.$item['value'].'</th>';
	}, $header);

?>

<div class="data-container">
	<div class="data-action">
		<div class="data-action-header">	
			<h2>Alumni Management</h2>
			<?php if (isset($_SESSION['message'])) { ?>
				<div
					class="alert alert-<?php echo $_SESSION['alert'] ?>"
					role="alert"
				>
				  <?php echo $_SESSION['message'] ?>
				  <button
				  	type="button"
				  	class="close"
				  	data-dismiss="alert"
				  	aria-label="Close"
				  >
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			<?php unset($_SESSION['message']); } ?>
		</div>
		<div class="data-action-button">
			<a
				href="/?page=alumni_create"
				class="btn btn-primary btn-sm"
			>
				<span class="fa fa-plus"></span>
				Add Alumni Record
			</a>
			<a
				href="/?page=import"
				class="btn btn-info btn-sm"
			>
				<span class="fa fa-upload"></span>
				Import XLS/XLSX
			</a>
			<a
				href="/?page=announce"
				class="btn btn-success btn-sm"
			>
				<span class="fa fa-bullhorn"></span>
				Send Announcement
			</a>
		</div>
	</div>
	<table class="table table-striped table-hover">
	  <thead>
	    <tr>
	    	<?php echo implode('', $parsedHeader) ?>
	    </tr>
	  </thead>
	  <tbody>
	  	<?php foreach($alumni as $datum)  {
	  		echo '<tr>'.$datum.'</tr>';
	  	} ?>
	  </tbody>
	</table>
	<div class="data-pagination">
		<nav aria-label="pp">
			<ul class="pagination">
				<?php $prev = $skip - 1; $next = $skip + 1; ?>
				<?php for($x = 0; $x < ceil($count / $take); $x++) 
				{
					$page = $x + 1;
					$active = $page == $skip ? ' active' : '';
					echo "<li class=\"page-item$active\"><a href=\"/?page=alumni&from=$page&perPage=$take\" class=\"page-link\">$page</a></li>";
				} ?>
			</ul>
		</nav>
	</div>
</div>