<?php

use App\Model\Alumni;
use App\Model\User;
use \Carbon\Carbon;

$emailConfig = getConfig()['email'];
$host = getConfig()['host'];

$alumni = Alumni::find($_GET['id']);

if (!count($alumni))
{
    $_SESSION['alert'] = 'warning';
    $_SESSION['message'] = 'Specified resources does not exist.';
		header('location: /?page=alumni');
		exit();
}

if (!!count($_POST)) 
{
	// Congratulations, your input has an email!
	if ($_POST['email'])
	{
		$record = Alumni::where('email', $_POST['email'])->where('id', '!=', $alumni->id)->get();

		if (!!count($record))
		{
			$_SESSION['alert'] = 'danger';
			$_SESSION['message'] = 'Email is already used.';
		} else {
			$_POST = array_map(function($item) {
				return htmlentities($item);
			}, $_POST);

			$status = $alumni->update($_POST);

			if ($status)
			{
				$_SESSION['alert'] = 'success';
				$_SESSION['message'] = 'You have successfully edited!';
			}	
		}
	}
	else
	{
		$_POST = array_map(function($item) {
			return htmlentities($item);
		}, $_POST);

		$status = $alumni->update($_POST);

		if ($status)
		{
			$_SESSION['alert'] = 'success';
			$_SESSION['message'] = 'You have successfully edited!';
		}
	}

	if($_POST['promote'] === "yes" && !$alumni->user)
	{
		if (!$alumni->email)
		{
			$_SESSION['alert'] = 'danger';
			$_SESSION['message'] = 'You have successfully edited the entity!<br />
			But we\'re not able to promote entity as user, please make sure that the entity has valid email';
		}
		else
		{
			$user = new User();
			$user->access_token = bin2hex(openssl_random_pseudo_bytes(10));
			$user->expires_on = (new Carbon())->addDays(7);
			$user->alumni_id = $alumni->id;
			$user->save();

			// send email...
			$from = new SendGrid\Email($emailConfig['sender']['name'], $emailConfig['sender']['email']);
			$subject = "Promoted as User!";
			$to = new SendGrid\Email($alumni->first_name.' '.$alumni->last_name, $alumni->email);
			$message = 'Please click <a href="'.$host.'/recover.php?code='.$user->access_token.'">here</a> to activate your account.';
			$content = new SendGrid\Content("text/html", $message);
			$mail = new SendGrid\Mail($from, $subject, $to, $content);
			$sg = new \SendGrid($emailConfig['api_key']);

			$response = $sg->client->mail()->send()->post($mail);

			if ($response->statusCode() === 200 || $response->statusCode() === 202)
			{
				$_SESSION['alert'] = 'success';
				$_SESSION['message'] = 'You have successfully edited and promoted alumni as user!';
			}
			else
			{
				$_SESSION['alert'] = 'warning';
				$_SESSION['message'] = 'Email not sent. Please retry.';
			}
		}
	}
	else
	{
		$user = User::where('alumni_id', $alumni->id)->first();

		if($user)
		{
			$user->delete();
		}
	}
}

?>

<div class="app-container">
	<div class="registration">
		<h2>Edit Alumni</h2>
		<?php if (isset($_SESSION['message'])) { ?>
			<div
				class="alert alert-<?php echo $_SESSION['alert'] ?>"
				role="alert"
			>
			  <?php echo $_SESSION['message'] ?>
			  <button
			  	type="button"
			  	class="close"
			  	data-dismiss="alert"
			  	aria-label="Close"
			  >
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
		<?php unset($_SESSION['message']); } ?>
		<form method="POST">
			<div class="registration-form">
				<div class="registration-element">
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">First name</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="first_name"
					  	id="first_name"
              value="<?php echo $alumni->first_name ?>"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Middle name</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="middle_name"
					  	id="middle_name"
              value="<?php echo $alumni->middle_name ?>"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Last name</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="last_name"
					  	id="last_name"
              value="<?php echo $alumni->last_name ?>"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Male</span>
					    <div class="input-group-text">
								<?php if ($alumni->gender === 'male') { ?>
									<input
										type="radio"
										id="gender"
										name="gender"
										value="male"
										checked
					      	>
								<?php } else { ?>
									<input
										type="radio"
										id="gender"
										name="gender"
										value="male"
					      	>
								<?php } ?>
					    </div>
					    <span class="input-group-text">Female</span>
					    <div class="input-group-text">
								<?php if ($alumni->gender === 'female') { ?>
									<input
										type="radio"
										id="gender"
										name="gender"
										value="female"
										checked
					      	>
								<?php } else { ?>
									<input
										type="radio"
										id="gender"
										name="gender"
										value="female"
					      	>
								<?php } ?>
					    </div>
					  </div>
					 </div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Email</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="email"
					  	id="email"
							value="<?php echo $alumni->email ?>"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Phone</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="contact"
					  	id="contact"
							value="<?php echo $alumni->contact ?>"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Promote as User</span>
							<span class="input-group-text">Yes</span>
					    <div class="input-group-text">
								<?php if ($alumni->user->id) { ?>
									<input
										type="radio"
										id="promote"
										name="promote"
										value="yes"
										checked
					      	>
								<?php } else { ?>
									<input
										type="radio"
										id="promote"
										name="promote"
										value="yes"
					      	>
								<?php } ?>
					    </div>
					    <span class="input-group-text">No</span>
					    <div class="input-group-text">
								<?php if (!$alumni->user->id) { ?>
									<input
										type="radio"
										id="promote"
										name="promote"
										value="no"
										checked
					      	>
								<?php } else { ?>
									<input
										type="radio"
										id="promote"
										name="promote"
										value="no"
					      	>
								<?php } ?>
					    </div>
						</div>
					</div>
				</div>
				<div class="registration-element">
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Year Graduated</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="graduated_on"
					  	id="graduated_on"
							value="<?php echo $alumni->graduated_on ?>"
					  >
					</div>
				  <div class="input-group  input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Awards</span>
					  </div>
					  <textarea
					  	class="form-control"
					  	id="awards"
					  	name="awards"
					  ><?php echo $alumni->awards ?></textarea>
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Employment Status</span>
					  </div>
					  <select
							class="form-control"
							id="employment_status"
							name="employment_status"
						>
							<?php if ($alumni->employment_status === 'permanent') { ?>
								<option value="permanent" selected>Permanent</option>
							<?php } else { ?>
								<option value="permanent">Permanent</option>
							<?php } ?>
							
							<?php if ($alumni->employment_status === 'temporary') { ?>
								<option value="temporary" selected>Temporary</option>
							<?php } else { ?>
								<option value="temporary">Temporary</option>
							<?php } ?>
						</select>
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Job</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="job"
					  	id="job"
							value="<?php echo $alumni->job ?>"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Company</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="company"
					  	id="company"
							value="<?php echo $alumni->company ?>"
					  >
					</div>
					<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Salary</span>
					  </div>
					  <input
					  	type="text"
					  	class="form-control"
					  	name="salary"
					  	id="salary"
							value="<?php echo $alumni->salary ?>"
					  >
					</div>
				</div>
			</div>
			<input
				type="submit"
				class="btn btn-primary btn-block"
				name="register"
				value="Submit"
			/>
		</form>
	</div>
</div>
		