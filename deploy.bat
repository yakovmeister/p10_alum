@echo off
REM you can specify which port you would run the app.
REM but by default it uses the 80 port
REM make sure nothing is using the port.
php -S localhost:80 -c resources/php.ini