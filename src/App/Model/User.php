<?php

namespace App\Model;

class User extends \Illuminate\Database\Eloquent\Model 
{
	public $timestamps = true;
	
	protected $guarded = [
		'password'
	];

	public function alumni()
	{
		return $this->hasOne("App\\Model\\Alumni");
	}
}