<?php

namespace App\Model;

class Alumni extends \Illuminate\Database\Eloquent\Model 
{
	public $timestamps = true;

	protected $table = 'alumni';

	protected $fillable = [
		'first_name',
		'middle_name',
		'last_name',
		'email',
		'contact',
		'gender',
		'graduated_on',
		'awards',
		'employment_status',
		'job',
		'company',
		'salary'
	];

	public function user()
	{
		return $this->belongsTo("App\\Model\\User", 'id', 'alumni_id');
	}
}