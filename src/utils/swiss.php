<?php

use \App\Model\User;
use \App\Model\Alumni;
use \Firebase\JWT\JWT;
use \Illuminate\Pagination\Paginator;

// lul
if(!function_exists('smsSend'))
{
	function smsSend($number, $message, $apicode = 'ST-SYDTA010360_7YJPG'){
		$url = 'https://www.itexmo.com/php_api/api.php';
		$itexmo = array('1' => $number, '2' => $message, '3' => $apicode);
		$param = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => http_build_query($itexmo),
		    ),
		);

		$context  = stream_context_create($param);
		return file_get_contents($url, false, $context);
	}
}

/**
 * Too lazy to create separate config file and module. So I'm gonna put it here... as a function.
 * @return array config fucking file.
 */
if (!function_exists("getConfig"))
{
	function getConfig() 
	{
		return [
			'host' => 'localhost',
			'sms' => [
				'email' => 'electro7bug@mailinator.com',
				'password' => 'abc123',
				'message' => 'Congratulations, you are now registered as Alumni! You may receive news and announcement from the school using this number.'
			],
			'email' => [
				'api_key' => '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',
				'sender' => [
					'name' => 'Admin',
					'email' => 'noreply@localhost'
				]
			]
		];
	}
}

/**
 * Generate navigation bar for the app
 * @param  $currentPage current page
 * @return  string generated navigation bar
 */
if (!function_exists('navigation')) 
{
	function navigation($currentPage, $title = 'Client')
	{
		return '
			<nav class="navbar navbar-expand-lg navbar-dark bg-primary navbar-fixed">
			  <a class="navbar-brand" href="/?page=index">'.$title.'</a>
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item '.($currentPage === 'alumni' ? 'active' : '').'">
		        <a class="nav-link" href="/?page=alumni"><span class="fa fa-graduation-cap"></span> Alumni Management</a>
		      </li>
		    </ul>
		    <ul class="navbar-nav">
		      <li class="nav-item">
		        <a class="nav-link" href="/?page=logout"><span class="fa fa-sign-out-alt"></span> Log out</a>
		      </li>
		    </ul>
			</nav>
	';
	}
}

/**
 * Attempts to authenticate user
 * @param  $email [login email]
 * @param  $password [unhashed/md5 password]
 * @return  mixed [boolean false if no user found, otherwise returns the user info]
 */
if(!function_exists("authenticate"))
{
    function authenticate($email, $password)
    {
			$record = Alumni::where('email', $email)->first();

			if (!count($record))
			{
				return false;
			}

			if ($record->user->password !== md5($password))
			{
				return false;
			}

			// static jwt key, because I'm not paid enough
			// to develop customization related features.
			$key = 'SomeRandomKeyIGuess?';

			$token = [
				'jti' => base64_encode(random_bytes(32)),
				'iat' => time(),
				'iss' => 'localhost',
				'data' => [
					'email' => $record->email,
					'id' => $record->id,
					'user_id' => $record->user->id
				]
			];

			$_SESSION['app-token'] = JWT::encode($token, $key);
			$_SESSION['uid'] = $record->id;

			return $record;
    }
}

/**
 * Checks whether a user is authenticated
 * @return bool [user id is set on session var]
 */
if(!function_exists("is_authenticated"))
{
    function is_authenticated()
    {
    	if (!isset($_SESSION['app-token']))
    	{
    		return false;
    	}

			// static jwt key, because I'm not paid enough
			// to develop customization related features.
			$key = 'SomeRandomKeyIGuess?';

    	$decodedToken = JWT::decode($_SESSION['app-token'], $key, array('HS256'));

    	$record = Alumni::find($decodedToken->data->id);

    	return !!count($record);
    }
}