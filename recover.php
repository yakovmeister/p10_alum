<?php
/**
 * Alumni Monitoring System
 * ------------------------
 * Disclaimer: due to fund shortage, most of the security features,
 * customization features, real time interaction are not developed.
 * If in any case client's mind has changed and wants the fully featured app,
 * you can shoot me an email at owari@waifu.club for pricing.
 * 
 * @author yakovmeister <owari@waifu.club>
 * @version 1.0.0
 */

error_reporting(0);
session_start();

require('vendor/autoload.php');

use \App\Database;
use \App\Notification\SMS;
use \App\Model\Alumni;
use \App\Model\User;
use \Carbon\Carbon;

// initialize our connection
$connection = new Database;
$emailConfig = getConfig()['email'];

$is_authenticated = is_authenticated();

if ($is_authenticated) 
{
	header('location: /?page=index');
	exit();
}

if ($_GET['code'])
{
	$user = User::where('access_token', $_GET['code'])->first();
	if (!$user)
	{
		$_SESSION['alert'] = 'danger';
		$_SESSION['message'] = 'Token is invalid or expired';
		header('location: /?page=index');
		exit();
	}
	
	if ($_POST)
	{
		if ($_POST['password'] !== $_POST['repeat_password'])
		{
			$_SESSION['alert'] = 'danger';
			$_SESSION['message'] = 'Password didn\'t match';
			header('refresh:0');
			exit();
		}
		else
		{
			if (!Alumni::find($user->alumni_id))
			{
				$_SESSION['alert'] = 'danger';
				$_SESSION['message'] = 'Email is not associated with any alumni.';
				header('location: /?page=index');
				exit();
			}

			$user->password = md5($_POST['password']);
			$user->access_token = null;
			$user->expires_on = null;
			$user->save();

			$_SESSION['alert'] = 'success';
			$_SESSION['message'] = 'Password reset successful';
			header('location: /?page=index');
			exit();
		}
	}
}
else
{
	if ($_POST)
	{
		$alumni = Alumni::where('email', $_POST['email'])->first();

		if (!count($alumni))
		{
			$_SESSION['alert'] = 'danger';
			$_SESSION['message'] = 'Email is not associated with any alumni.';
		}
		else
		{
			$user = User::where('alumni_id', $alumni->id);
			$user->access_token = bin2hex(openssl_random_pseudo_bytes(10));
			$user->expires_on = (new Carbon())->addDays(7);
			$user->save();

			$from 		= new SendGrid\Email($emailConfig['sender']['name'], $emailConfig['sender']['email']);
			$subject 	= "Forgot Paasuwaado";
			$to 			= new SendGrid\Email($alumni->first_name.' '.$alumni->last_name, $alumni->email);
			$message 	= 'Please click <a href="'.$host.'/recover.php?code='.$user->access_token.'">here</a> to reset your password.';
			$content 	= new SendGrid\Content("text/html", $message);
			$mail 		= new SendGrid\Mail($from, $subject, $to, $content);
			$sg 			= new \SendGrid($emailConfig['api_key']);
			$response = $sg->client->mail()->send()->post($mail);

			if ($response->statusCode() === 200 || $response->statusCode() === 202)
			{
				$_SESSION['alert'] = 'success';
				$_SESSION['message'] = 'Email sent successfully.';
			}
			else
			{
				$_SESSION['alert'] = 'warning';
				$_SESSION['message'] = 'Email not sent. Please retry.';
			}
		}
	}
}

?>

<?php /** Our base HTML Template */ ?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-reboot.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/fontawesome.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	</head>
	<body class="app-container">
		<div class="registration">
			<?php if (isset($_SESSION['message'])) { ?>
				<div
					class="alert alert-<?php echo $_SESSION['alert'] ?>"
					role="alert"
				>
				  <?php echo $_SESSION['message'] ?>
				  <button
				  	type="button"
				  	class="close"
				  	data-dismiss="alert"
				  	aria-label="Close"
				  >
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			<?php unset($_SESSION['message']); } ?>
			<?php if ($_GET['code']) { ?>
				<h2>Reset Password:</h2>
				<form method="POST">
					<div class="registration-form">
						<div class="registration-element">
							<input
								class="form-control"
								name="password"
								id="password"
								type="password"
								placeholder="Password"
							/>
						</div>
						<div class="registration-element">
							<input
								class="form-control"
								name="repeat_password"
								id="repeat_password"
								type="password"
								placeholder="Repeat Password"
							/>
						</div>
					</div>
					<input
						type="submit"
						class="btn btn-primary btn-block"
						name="register"
						value="Submit"
					/>
				</form>
			<?php } else { ?>
			<h2>Forgot Password</h2>
			<form method="POST">
				<div class="registration-form">
					<div class="registration-element">
						<input
              class="form-control"
              name="email"
              id="email"
              type="text"
              placeholder="e.g. john69@email.com"
	          />
					</div>
				</div>
				<input
					type="submit"
					class="btn btn-primary btn-block"
					name="register"
					value="Submit"
				/>
			</form>
			<?php } ?>
		</div>
		<script type="text/javascript" src="assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/js/popper.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	</body>
</html>