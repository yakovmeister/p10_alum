# Alumni Information Database  
  
## Prerequisite  
* PHP >= 7 (php must be runnable using php command in your Terminal/cmd)
* composer (getcomposer.org)
* smsgateway.me android app

## Dependencies
* Illuminate/Database - 4.2.*
* PHPOffice/PHPSpreadsheet - 1.1.0
* firebase/php-jwt - ^5.0
* sendgrid/sendgrid - dev-masters

## Setup
* Install smsgateway.me app in your android device
* if vendor path exists, you can run deploy.bat
* otherwise run composer install first then back to step two.
* for the email links to work, you must change the host config from src/App/utils/swiss.php

## Directory Structure
* assets - is where all of front end related (CSS, JS, Image, fonts) are located.
* pages - this are the pages of the app. called inside the index.php
* resources - NEVER DELETE THIS DIRECTORY: contains database, configuration file and upload directory
* sample_format - can be deleted, path that contains a sample format for imports
* src - PSR-4 complaint codes
* vendor - Third party codes (must not be deleted or moved, autoloading feature may not work)
