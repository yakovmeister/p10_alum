<?php
/**
 * Alumni Monitoring System
 * ------------------------
 * Disclaimer: due to fund shortage, most of the security features,
 * customization features, real time interaction are not developed.
 * If in any case client's mind has changed and wants the fully featured app,
 * you can shoot me an email at owari@waifu.club for pricing.
 * 
 * @author yakovmeister <owari@waifu.club>
 * @version 1.0.0
 */

error_reporting(0);
session_start();

require('vendor/autoload.php');

use App\Database;

// initialize our connection
$connection = new Database;

$is_authenticated = is_authenticated();

/**
 * Setup single-page routing using query strings.
 */
if ($is_authenticated) {
	$_GET['page'] = $_GET['page'] !== 'login'
		? $_GET['page']
		: 'index';
} else {
	$_GET['page'] = 'login';
}

$_GET['page'] = $_GET['page']
	? preg_replace('/[^a-zA-Z0-9 _-]/', '', $_GET['page'])
	: 'index';

	//ST-SYDTA010360_7YJPG

?>

<?php /** Our base HTML Template */ ?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-reboot.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/fontawesome.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/chartist.css">
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	</head>
	<body>
		<?php if ($is_authenticated) echo navigation($_GET['page'], 'ICTDB') ?>
		<?php

			if (file_exists('pages/'.$_GET['page'].'.php'))
			{
				require('pages/'.$_GET['page'].'.php');
			}
			else 
			{
				require('pages/notfound.php');
			}
		?>
		<script type="text/javascript" src="assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/js/popper.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	</body>
</html>